from django.db import models

# Create your models here.
class UserLevel(models.Model):
    name = models.CharField(max_length=50)
    desc = models.TextField(max_length=250)

    def __str__(self):
        return self.name;

class Gender(models.Model):
    code = models.CharField(max_length=1)
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name;

class Country(models.Model):
    code = models.CharField(max_length=2)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name;

class Region(models.Model):
    country = models.ForeignKey(Country)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name;

class City(models.Model):
    region = models.ForeignKey(Region)
    country = models.ForeignKey(Country)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name;

class Note(models.Model):
    text =  models.TextField(max_length=5000)
    title = models.CharField(max_length=250)

    def __str__(self):
        return self.title;

class Meeting(models.Model):
    title = models.CharField(max_length=250)
    date = models.DateField(blank=False)
    time = models.TimeField(blank=False)
    desc = models.TextField(max_length=1000)

class Taxonomy(models.Model):
    name = models.CharField(max_length=50)
    desc = models.TextField(max_length=500)

    def __str__(self):
        return self.name;

class ConsultingService(models.Model):
    name = models.CharField(max_length=150)
    desc = models.TextField(max_length=500)

class BusinessProposal(models.Model):
    name = models.CharField(max_length=150)
    desc = models.TextField(max_length=1000)
    consulting_service = models.ForeignKey(ConsultingService)

class User(models.Model):
    user_level = models.ForeignKey(UserLevel)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    pub_date = DateTimeField('date created')
    gender = models.ForeignKey(Gender)
    country = models.ForeignKey(Country)
    region = models.ForeignKey(Region)
    city = models.ForeignKey(City)

class Company(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField(max_length=200)
    pub_date = DateTimeField('date created')
    note = models.ForeignKey(Note)
    meeting = models.ForeignKey(Meeting)
    consultant = models.ForeignKey(User)
    consulting_service = models.ForeignKey(ConsultingService)
    bsuiness_proposal = models.ForeignKey(BusinessProposal)
    taxonomy = models.ManyToManyField(Taxonomy, related_name="tags")

    def __str__(self):
        return self.name;
