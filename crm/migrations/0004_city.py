# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_region'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('country', models.ForeignKey(to='crm.Country')),
                ('region', models.ForeignKey(to='crm.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
