# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0004_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessProposal',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=150)),
                ('desc', models.TextField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('desc', models.TextField(max_length=200)),
                ('bsuiness_proposal', models.ForeignKey(to='crm.BusinessProposal')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConsultingService',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=150)),
                ('desc', models.TextField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Meeting',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('title', models.CharField(max_length=250)),
                ('date', models.DateField()),
                ('time', models.TimeField()),
                ('desc', models.TextField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('text', models.TextField(max_length=5000)),
                ('title', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Taxonomy',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('desc', models.TextField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('city', models.ForeignKey(to='crm.City')),
                ('country', models.ForeignKey(to='crm.Country')),
                ('region', models.ForeignKey(to='crm.Region')),
                ('sex', models.ForeignKey(to='crm.Sex')),
                ('user_level', models.ForeignKey(to='crm.UserLevel')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='company',
            name='consultant',
            field=models.ForeignKey(to='crm.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='consulting_service',
            field=models.ForeignKey(to='crm.ConsultingService'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='meeting',
            field=models.ForeignKey(to='crm.Meeting'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='note',
            field=models.ForeignKey(to='crm.Note'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='taxonomy',
            field=models.ManyToManyField(to='crm.Taxonomy', related_name='tags'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='businessproposal',
            name='consulting_service',
            field=models.ForeignKey(to='crm.ConsultingService'),
            preserve_default=True,
        ),
    ]
